import cv2
import os
import time
import datetime
import common
import numpy as np
from estimator import TfPoseEstimator
from networks import get_graph_path
from lifting.prob_model import Prob3dPose

def FeatureExtractor():
    ArmFeature = 0
    NeckFeature = 0
    KneeFeature = 0
    name = 'C:/Users/manos/Desktop/tesi/tf-openpose/gaits/gait1/frame.jpg'
    w = 432
    h = 368
    e = TfPoseEstimator(get_graph_path('mobilenet_thin_432x368'), target_size=(w, h))
    image = common.read_imgfile(name, w, h)
    humans = e.inference(image)
    image = TfPoseEstimator.draw_humans(image, humans, imgcopy=False)
    poseLifting = Prob3dPose('./src/lifting/models/prob_model_params.mat')
    image_h, image_w = image.shape[:2]
    standard_w = 640
    standard_h = 480
    pose_2d_mpiis = []
    visibilities = []
    for human in humans:
        pose_2d_mpii, visibility = common.MPIIPart.from_coco(human)
        pose_2d_mpiis.append([(int(x * standard_w + 0.5), int(y * standard_h + 0.5)) for x, y in pose_2d_mpii])
        visibilities.append(visibility)
    pose_2d_mpiis = np.array(pose_2d_mpiis)
    visibilities = np.array(visibilities)
    transformed_pose2d, weights = poseLifting.transform_joints(pose_2d_mpiis, visibilities)
    pose_3d = poseLifting.compute_3d(transformed_pose2d, weights)
    pose_3dqt = np.array(pose_3d[0]).transpose()
    #print(pose_3dqt)
    #in pose_3dqt[r][c] r=riga, c=colonna
    os.system('cls')
    """calcolo della posizione del naso rispetto al collo (per capire se cammina a testa curva)"""
    x1 = abs(pose_3dqt[0][0]) #prendo la coordinata X del naso
    x2 = abs(pose_3dqt[1][0]) #prendo la coordinata X del collo
    frazione = abs(x1 - x2) / abs(x1 + x2)
    if frazione >= 0.20: #con 0.25 R=79%
        NeckFeature +=1
    """calcolo della posizione del gomito rispetto al polso (per capire se quando cammina ha il braccio inclinato)"""
    da_polso_a_gomito_dx_asseX = abs(pose_3dqt[3][0]) - abs(pose_3dqt[4][0]) #prendo la distanza in coordinate X dal gomito al polso destro
    da_polso_a_gomito_sx_asseX = abs(pose_3dqt[6][0]) - abs(pose_3dqt[7][0]) #prendo la distanza in coordinate X dal gomito al polso sinistro
    denomD_X = abs(abs(pose_3dqt[3][0]) + abs(pose_3dqt[4][0]))
    denomS_X = abs(abs(pose_3dqt[6][0]) + abs(pose_3dqt[7][0]))
    frazioneSX = abs(abs(da_polso_a_gomito_dx_asseX) / denomD_X)
    frazioneDX = abs(abs(da_polso_a_gomito_sx_asseX) / denomS_X)
    if frazioneDX >= 0.30 and frazioneSX >= 0.40:
        ArmFeature += 1
    else:
        if frazioneDX >= 0.40 and frazioneSX >= 0.30:
            ArmFeature += 1
    """calcolo della posizione dell'anca rispetto al ginocchio (per capire se quando cammina flette le ginocchia)"""
    da_anca_a_ginocchio_dx_asseX = abs(pose_3dqt[8][0]) - abs(pose_3dqt[9][0])
    da_anca_a_ginocchio_sx_asseX = abs(pose_3dqt[11][0]) - abs(pose_3dqt[12][0])
    denom_dx = abs(pose_3dqt[8][0]) + abs(pose_3dqt[9][0])
    denom_sx = abs(pose_3dqt[11][0]) + abs(pose_3dqt[12][0])
    r_dx = abs(da_anca_a_ginocchio_dx_asseX/denom_dx)
    r_sx = abs(da_anca_a_ginocchio_sx_asseX/denom_sx)
    if(r_dx >= 0.30) and (r_sx >= 0.30):
        KneeFeature += 1
    os.system('cls')
    return(NeckFeature, ArmFeature, KneeFeature)

startingTime = time.time()
cv2.setUseOptimized(True)
dir = 'C:/Users/manos/Desktop/tesi/tf-openpose/gaits/gait1/Parkinsonian_Gait.mp4'
frames_dir = 'C:/Users/manos/Desktop/tesi/tf-openpose/gaits/gait1/'
cap = cv2.VideoCapture(dir)
NUMBER_OF_FRAMES = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
NF = 0
AF = 0
KF = 0
currentFrame = 0
ArmFeature = 0
NeckFeature = 0
KneeFeature = 0
nFiles = len([name for name in os.listdir('C:/Users/manos/Desktop/tesi/tf-openpose/gaits/gait1/')])
if(nFiles==2):
    print("Skipping video segmentation (already done)")
    cap.release()
else:
    try:
        if not os.path.exists(frames_dir):
            os.makedirs(frames_dir)
    except OSError:
        print ('Error: Could not create video_frames folder')
    try:
        if (cap.isOpened()== False): 
            cap.open()
    except IOError:
        print("Error opening video stream or file")
    while(currentFrame < NUMBER_OF_FRAMES):
        ret, frame = cap.read()
        name = 'C:/Users/manos/Desktop/tesi/tf-openpose/gaits/gait1/frame.jpg'
        cv2.imwrite(name, frame)
        print("Analyzing frame: " + str(currentFrame) + "...")
        NF, AF, KF = FeatureExtractor()
        NeckFeature += NF
        ArmFeature += AF
        KneeFeature += KF
        currentFrame += 1
    cap.release()
    cv2.destroyAllWindows()
    NeckFeature = ((NeckFeature * 100) / (currentFrame + 1))
    ArmFeature = ((ArmFeature * 100) / (currentFrame + 1))
    KneeFeature = ((KneeFeature * 100) / (currentFrame + 1))
    print("Upper body inclination rate \tR=" + str(NeckFeature))
    print("Arms inclination rate       \tR=" + str(ArmFeature))
    print("Knees inclination rate      \tR=" + str(KneeFeature))
    finalRecognitionRate = ((NeckFeature + ArmFeature + KneeFeature) / 3)
    if finalRecognitionRate >= 70.0:
        print("The patient is a PD - with R=" + str(finalRecognitionRate) + " -\nNumber of frames analyzed: " + str(currentFrame + 1))
    else:
        print("The patient is healthy\nNumber of frames analyzed: " + str(currentFrame + 1))
    os.remove('C:/Users/manos/Desktop/tesi/tf-openpose/gaits/gait1/frame.jpg')
    endingTime = time.time()
    elapsedTime = endingTime - startingTime
    print("Elapsed time:\t" + str(datetime.timedelta(seconds=round(elapsedTime))))