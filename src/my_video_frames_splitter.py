import cv2
import os
import common
import numpy as np
from estimator import TfPoseEstimator
from networks import get_graph_path
from lifting.prob_model import Prob3dPose
import time
import datetime

startingTime = time.time()
cv2.setUseOptimized(True)
dir = 'C:/Users/manos/Desktop/tesi/tf-openpose/gaits/gait1/gait_video/Parkinsonian_Gait.mp4'
frames_dir = 'C:/Users/manos/Desktop/tesi/tf-openpose/gaits/gait1/gait_frames/'
cap = cv2.VideoCapture(dir)
NUMBER_OF_FRAMES = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
currentFrame = 0
NeckFeature = 0
ArmFeature = 0
distance_coeff = 1
nFiles = len([name for name in os.listdir("C:/Users/manos/Desktop/tesi/tf-openpose/gaits/gait1/gait_frames")])
if(nFiles>0):
    print("Skipping video segmentation (already done)")
    cap.release()
else:
    try:
        if not os.path.exists(frames_dir):
            os.makedirs(frames_dir)
    except OSError:
        print ('Error: Could not create video_frames folder')
    try:
        if (cap.isOpened()== False): 
            cap.open()
    except IOError:
        print("Error opening video stream or file")
    while(currentFrame < NUMBER_OF_FRAMES):
        # Capture frame-by-frame
        ret, frame = cap.read()
        # Saves image of the current frame in jpg file
        name = 'C:/Users/manos/Desktop/tesi/tf-openpose/gaits/gait1/gait_frames/frame' + str(currentFrame) + '.jpg'
        print('Creating...' + name)
        cv2.imwrite(name, frame)
        currentFrame += 1
    cap.release()
    cv2.destroyAllWindows()

def metodo(currentFrame):
    NeckFeature = 0
    ArmFeature = 0
    image = None
    e = None
    pose_3dqt = None
    name = 'C:/Users/manos/Desktop/tesi/tf-openpose/gaits/gait1/gait_frames/frame' + str(currentFrame) + '.jpg'
    print("Examining image: frame" + str(currentFrame) + ".jpg")
    w = 432
    h = 368
    e = TfPoseEstimator(get_graph_path('mobilenet_thin_432x368'), target_size=(w, h))
    image = common.read_imgfile(name, w, h)
    humans = e.inference(image)
    image = TfPoseEstimator.draw_humans(image, humans, imgcopy=False)
    poseLifting = Prob3dPose('./src/lifting/models/prob_model_params.mat')
    image_h, image_w = image.shape[:2]
    standard_w = 640
    standard_h = 480
    pose_2d_mpiis = []
    visibilities = []
    for human in humans:
        pose_2d_mpii, visibility = common.MPIIPart.from_coco(human)
        pose_2d_mpiis.append([(int(x * standard_w + 0.5), int(y * standard_h + 0.5)) for x, y in pose_2d_mpii])
        visibilities.append(visibility)
    pose_2d_mpiis = np.array(pose_2d_mpiis)
    visibilities = np.array(visibilities)
    transformed_pose2d, weights = poseLifting.transform_joints(pose_2d_mpiis, visibilities)
    pose_3d = poseLifting.compute_3d(transformed_pose2d, weights)
    pose_3dqt = np.array(pose_3d[0]).transpose()
    os.system('cls')
    x1 = abs(pose_3dqt[0][0]) #prendo la coordinata X del naso
    x2 = abs(pose_3dqt[1][0]) #prendo la coordinata X del collo
    if(abs(x1-x2)) <= 60*distance_coeff and (abs(x1-x2)) >= -60*distance_coeff: #se distano tra loro (-60,+60) punti
        NeckFeature += 1
    da_polso_a_gomito_dx_asseY=abs(pose_3dqt[3][1]) - abs(pose_3dqt[4][1]) #prendo la distanza in coordinate Y dal gomito al polso destro
    da_polso_a_gomito_sx_asseY=abs(pose_3dqt[6][1]) - abs(pose_3dqt[7][1]) #prendo la distanza in coordinate Y dal gomito al polso sinistro
    if(abs(da_polso_a_gomito_dx_asseY-da_polso_a_gomito_sx_asseY)) <= 60*distance_coeff and (abs(da_polso_a_gomito_dx_asseY-da_polso_a_gomito_sx_asseY)) >= -60*distance_coeff: #se entrambe le distanze distano (-60,+60)
        ArmFeature += 1
    cv2.destroyAllWindows()
    os.system('cls')
    return(NeckFeature, ArmFeature)

cf = 0
s1 = 0
s2 = 0
while(cf < NUMBER_OF_FRAMES):
    f1, f2 = metodo(cf)
    s1 += f1
    s2 += f2
    cf +=1
print("Upper body inclination rate \tR=" + str(((s1*100)/NUMBER_OF_FRAMES)))
print("Arms inclination rate \tR=" + str(((s2*100)/NUMBER_OF_FRAMES)))
endingTime = time.time()
elapsedTime = endingTime - startingTime
print("Elapsed time:\t" + str(datetime.timedelta(seconds=round(elapsedTime))))